#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <fstream>
#include <thread>
#include <mutex>
using namespace std;

class MessagesSender
{
private:
	vector<string> _connected;
	queue<string> _lines;
	string _dataFile;

	void SignIn();
	void SignOut();
public:
	MessagesSender(string);
	~MessagesSender();

	void GetData();
	void SendMessages();
	void DisplayMenu();
};

