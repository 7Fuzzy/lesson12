#include "MessagesSender.h"

#define SIGN_IN 1
#define SIGN_OUT 2
#define CON_USERS 3
#define EXIT 4

#define WAIT_TIME 10000 //30000 for 0.5 min

mutex conM; //user list's lock
mutex dataM; //lines queue's lock
condition_variable conData; //lines queue's condition variable

//Building a Messages sender with given path for data file
MessagesSender::MessagesSender(string data)
{
	_dataFile = data;
}


MessagesSender::~MessagesSender()
{
}

//Function displays the menu and handles the choices
void MessagesSender::DisplayMenu()
{
	int choice = -1;

	while (choice != EXIT)
	{
		cout << "1.) Sign In" << endl;
		cout << "2.) Sign Out" << endl;
		cout << "3.) Connected Users" << endl;
		cout << "4.) Exit" << endl;

		cin >> choice;

		switch (choice)
		{
		case SIGN_IN:
			this->SignIn();
			break;
		case SIGN_OUT:
			this->SignOut();
			break;
		case CON_USERS:
			cout << "Connected Users: " << endl;
			for (auto e : this->_connected)
			{
				cout << e << endl;
			}
			break;
		case EXIT:
			break;
		default:
			cout << "Invalid Choice" << endl;
			break;
		}
	}
}

//Function adds a user to the list if its not already in there
void MessagesSender::SignIn()
{
	string name = "";
	cout << "Enter Name: ";
	cin >> name;



	if (find(this->_connected.begin(), this->_connected.end(), name) == this->_connected.end())
	{
		unique_lock<mutex> conLock(conM);
		this->_connected.push_back(name);
	}
	else
	{
		cout << "User Already signed in" << endl;
	}
}

//Function removes a user from the list if its there
void MessagesSender::SignOut()
{
	string name = "";
	cout << "Enter Name: ";
	cin >> name;



	if (find(this->_connected.begin(), this->_connected.end(), name) != this->_connected.end())
	{
		unique_lock<mutex> conLock(conM);
		this->_connected.erase(find(this->_connected.begin(), this->_connected.end(), name));
	}
}

//Function reads the data file and storing it's lines in the queue
void MessagesSender::GetData()
{
	while (true)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(WAIT_TIME)); //I did the waiting first so youll have time to sign in a few users

		fstream f;
		f.open(this->_dataFile);
		string line = "";
		while (getline(f, line))
		{
			unique_lock<mutex> dataLock(dataM); //locks the queue
			this->_lines.push(line);
			dataLock.unlock(); 
			conData.notify_one(); //notifies a waiting thread it can use the queue now
		}
		f.close();
		f.open(this->_dataFile, fstream::out | fstream::trunc); //clears the file
		f.close();

		
	}
}

//Function "sends" all of the users messages
void MessagesSender::SendMessages()
{
	while (true)
	{
		unique_lock<mutex> dataLock(dataM);
		
		conData.wait(dataLock, [=]() {return !this->_lines.empty(); }); //if line is empty keep waiting even if lock is unlocked
		

		string line = this->_lines.front();
		this->_lines.pop();
		dataLock.unlock();

		fstream f;
		f.open("output.txt", fstream::out | fstream::app);

		//Sending one line for all users
		for (int i = 0; i < this->_connected.size(); i++)
		{
			string u = this->_connected[i];

			f << u << ": " << line << endl;
		}

		f.close();

	}
}