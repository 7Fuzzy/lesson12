#include "MessagesSender.h"

int main()
{
	MessagesSender msg = MessagesSender("data.txt");
	thread menTrd(&MessagesSender::DisplayMenu, &msg);
	thread datTrd(&MessagesSender::GetData, &msg);
	thread sendTrd(&MessagesSender::SendMessages, &msg);
	datTrd.detach();
	sendTrd.detach();
	menTrd.join(); //we are waiting only for the menu, its the main thread
	
	return 0;
}